const mongoose = require('mongoose');

const ContactSchema = new mongoose.Schema({
  fullname: {
    type: String,
    required: true,
    trim: true,
    maxlength: 100
  },
  phone: {
    type: String
  },
  email: {
    type: String,
    required: true,
    trim: true,
    maxlength: 255
  },
  preferredDate: {
    type: Date
  },
  service: {
    type: String,
    trim: true,
    maxlength: 100
  },
  source: {
    type: String,
    maxlength: 100
  },
  message: {
    type: String,
    required: true,
    trim: true,
    maxlength: 255
  },
  date: {
    type: Date
  }
});

module.exports =
  mongoose.model.Contact || mongoose.model('Contact', ContactSchema);
