import mailgun from 'mailgun.js';

const mg = mailgun.client({
  username: 'api',
  key: process.env.NEXT_PUBLIC_MG_KEY
});

async function mailgunConnect({ fullname, phone, email, message }) {
  await mg.messages
    .create(process.env.NEXT_PUBLIC_MG_DOMAIN, {
      from: 'Conversion Whale <noreply@mg.thatwhale.com>',
      to: ['chris@conversionwhale.com'],
      subject: 'New Lead!',
      text: `Full Name: ${fullname}\nPhone: ${phone}\nEmail: ${email}\nMessage: ${message}\n`,
      'o:tracking': 'yes',
      'o:tracking-opens': 'yes'
    })
    .then(msg => console.log(msg)) // logs response data
    .catch(err => console.log(err)); // logs any error
}

export default mailgunConnect;
