import fetch from 'isomorphic-unfetch';

const serverEnv = `${process.env.VERCEL_URL}/api/sendcontact`;
const localEnv = 'http://localhost:3000/api/sendcontact';

async function storeContact(data) {
  try {
    await fetch(`${process.env.VERCEL_URL ? serverEnv : localEnv}`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data)
    });
  } catch (error) {
    console.log(error);
  }
}

export default storeContact;
