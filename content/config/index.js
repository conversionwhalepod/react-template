export const config = [
  {
    office: {
      office: 'Oak Park Dental Associates',
      phone: '(708) 848-8237',
      address: {
        street: '6711 West North Avenue',
        city: 'Oak Park',
        state: 'IL',
        zip: '60302',
        fullState: 'Illinois',
        directions:
          'We are located one block east of Oak Park Avenue on North Avenue.',
        iframeURL:
          'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2969.255868878034!2d-87.79546768455795!3d41.908857979219725!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x616e325c1f2189b3!2sOak%20Park%20Dental%20Associates!5e0!3m2!1sen!2sgt!4v1590602754302!5m2!1sen!2sgt',
        mapLink: 'https://www.google.com/maps?cid=7020604240357853619'
      },
      imgFile: 'oak-park-dental-associates',
      sinceYear: 2005,
      hours: [
        {
          Sunday: 'Closed',
          open: false
        },
        {
          Monday: 'Closed',
          open: false
        },
        {
          Tuesday: '9:30am - 6:00pm',
          open: true
        },
        {
          Wednesday: 'By Appointment Only',
          open: true
        },
        {
          Thursday: '9:30am - 6:00pm',
          open: true
        },
        {
          Friday: 'Closed',
          open: false
        },
        {
          Saturday: '8:30am - 3:00pm*',
          open: true
        }
      ],
      hoursNotes: '* Saturday office hours alternate every other week.',
      bookingMethod: 'localmed'
    },
    doctors: [
      {
        firstName: 'Nathaniel',
        lastName: 'Lim',
        fullName: 'Dr. Nathaniel Lim',
        prefix: 'Dr.',
        suffix: 'DDS',
        photoJPG: '/img/team/dr-lim.jpg',
        photoWEBP: '/img/team/dr-lim.webp',
        mainDoctor: true
      }
    ],
    doctorInfo:
      'At Oak Park Dental Associates, our top priority is to provide our patients with the knowledge to better their oral health.',
    social: {
      googlereview:
        'https://search.google.com/local/writereview?placeid=ChIJrTIlLEbLD4gRs4khH1wybmE',
      yelpreview:
        'https://www.yelp.com/biz/oak-park-dental-associates-oak-park',
      fanpage: 'oakparkdentalassociates',
      facebook: 'https://www.facebook.com/oakparkdentalassociates/',
      reviewNumber: 400,
      stars: 4
    },
    testimonials: [
      {
        name: 'Kamila D.',
        image: '/img/testimonials/oak-park-dental-associates-photo1.png',
        imageWebp: '/img/testimonials/oak-park-dental-associates-photo1.webp',
        review:
          'Very knowledgeable doctor, detail-oriented, dedicated to his work. I highly  recommend it.',
        reviewSite: 'google'
      },
      {
        name: 'Allie S.',
        image: '/img/testimonials/oak-park-dental-associates-photo2.png',
        imageWebp: '/img/testimonials/oak-park-dental-associates-photo2.webp',
        review:
          'Dr. Lim and his staff go above and beyond for their patients. I would highly recommend his office!',
        reviewSite: 'google'
      },
      {
        name: 'Judy W.',
        image: '/img/testimonials/oak-park-dental-associates-photo3.png',
        imageWebp: '/img/testimonials/oak-park-dental-associates-photo3.webp',
        review:
          'Dr. Lim is great in an emergency and is gentle and really cares. Both my husband and I are longtime clients and will continue to do so.',
        reviewSite: 'google'
      }
    ],
    specials: [
      {
        name: 'Emergency Exam',
        price: '$59',
        details: 'Includes exam and necessary x-rays.',
        notes: 'Limited time offer. New patients only.',
        free: false,
        discount: false
      },
      {
        name: 'Veneers',
        price: '10% OFF',
        details: 'Includes exam and necessary x-rays.',
        notes: 'Limited time offer. New patients only.',
        free: false,
        discount: true
      },
      {
        name: 'Implant Consultation',
        price: 'FREE',
        details: 'Includes exam and necessary x-rays.',
        notes: 'Limited time offer. New patients only.',
        free: true,
        discount: false
      },
      {
        name: 'Teeth Whitening Trays',
        price: '$99',
        details: 'Must mention this coupon at booking.',
        notes: 'Limited time offer. New patients only.',
        free: false,
        discount: false
      }
    ]
  }
];
