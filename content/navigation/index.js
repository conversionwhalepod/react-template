export const headerNav = [
  // Home Link
  {
    title: 'Home ',
    path: '/'
  },
  // Get to Know Our Office
  {
    title: 'Get to Know Our Office ',
    children: [
      {
        title: 'Your First Visit',
        path: '/your-first-visit',
        icon: 'frontdesk'
      },
      {
        title: 'Meet Our Doctor',
        path: '/our-doctor',
        icon: 'doctor'
      },
      {
        title: 'About Us',
        path: '/about-us',
        icon: 'about'
      },
      {
        title: 'What We Do',
        path: '/services',
        icon: 'sedation'
      },
      {
        title: 'Specials',
        path: '/specials',
        icon: 'specials'
      },
      {
        title: 'Office Tour',
        path: '/office-tour',
        icon: 'gallery'
      },
      {
        title: 'Addressing COVID-19',
        path: '/covid19',
        icon: 'clean-hands'
      }
    ]
  },
  {
    title: 'Our Services ',
    children: [
      {
        title: 'Dental Crowns',
        path: '/dental-crowns-oak-park'
      },
      {
        title: 'Dental Implants',
        path: '/dental-implants-oak-park'
      },
      {
        title: 'Emergency Dentistry',
        path: '/emergency-dentistry-oak-park'
      },
      {
        title: 'Family Dentistry',
        path: '/family-dentistry-oak-park'
      },
      {
        title: 'General Dentistry',
        path: '/general-dentistry-oak-park'
      },
      {
        title: 'Sleep Apnea Treatment',
        path: '/snoring-relief-sleep-apnea-treatment-oak-park'
      },
      {
        title: 'Teeth Whitening',
        path: '/teeth-whitening-oak-park'
      },
      {
        title: 'TMJ Treatment & Bite Problems',
        path: '/tmj-treatment-bite-problems-oak-park'
      }
    ]
  },
  // Location & Hours
  {
    title: 'Location & Hours ',
    path: '/office-hours'
  },
  // Contact Us
  {
    title: 'Contact Us ',
    path: '/contact-us'
  }
];

export const footerNav = [
  {
    columns: [
      {
        columnTitle: 'SERVICES',
        columnLinks: [
          { title: 'Dental Crowns', link: '/dental-crowns-oak-park' },
          { title: 'Dental Implants', link: '/dental-implants-oak-park' },
          {
            title: 'Emergency Dentistry',
            link: '/emergency-dentistry-oak-park'
          },
          { title: 'Family Dentistry', link: '/family-dentistry-oak-park' },
          { title: 'General Dentistry', link: '/general-dentistry-oak-park' },
          { title: 'Smile Makeover', link: '/smile-makeover-oak-park' },
          {
            title: 'Sleep Apnea Treatment',
            link: '/snoring-relief-sleep-apnea-treatment-oak-park'
          },
          {
            title: 'TMJ Treatment & Bite Problems',
            link: '/tmj-treatment-bite-problems-oak-park'
          },
          {
            title: 'View All Services',
            link: '/services'
          }
        ]
      },
      {
        columnTitle: 'OVERVIEW',
        columnLinks: [
          { title: 'First Visit', link: '/your-first-visit' },
          { title: 'About Us', link: '/about-us' },
          { title: 'Services', link: '/services' },
          { title: 'Specials', link: '/specials' },
          { title: 'Our Doctor', link: '/our-doctor' },
          { title: 'Testimonials', link: '/testimonials' }
        ]
      },
      {
        columnTitle: 'SUPPORT',
        columnLinks: [
          { title: 'Contact Us', link: '/contact-us' },
          { title: 'Location & Hours', link: '/office-hours' },
          { title: 'Financing', link: '/services' },
          { title: 'Review Us', link: '/specials' },
          { title: 'Addressing COVID-19', link: '/our-doctor' }
        ]
      }
    ]
  }
];
