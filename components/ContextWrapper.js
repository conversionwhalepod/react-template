import useModal from '../hooks/useModal';

import ConfigContext from '@/contexts/ConfigContext';
import ModalContext from '@/contexts/ModalContext';

function ContextWrapper({ children, configContext }) {
  const [open, handleClose, handleOpen] = useModal();
  return (
    <ConfigContext.Provider value={{ configContext }}>
      <ModalContext.Provider value={{ open, handleClose, handleOpen }}>
        {children}
      </ModalContext.Provider>
    </ConfigContext.Provider>
  );
}

export default ContextWrapper;
