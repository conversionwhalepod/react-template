import Link from 'next/link';
import { useContext } from 'react';
import ConfigContext from '@/contexts/ConfigContext';
import { SlideLeft, FadeIn } from './Common';

function HomeMeet() {
  const { configContext } = useContext(ConfigContext);
  return (
    <>
      <section className="meet meet-single">
        <div className="">
          <div className="meet-slider__slide">
            <div className="row meet-slider__slide--row">
              <div className="col-md-4 vcenter meet__doctor--image">
                <FadeIn>
                  <picture>
                    <source srcSet="/img/team/dr-lim-meet.webp" type="image/webp" />
                    <source srcSet="/img/team/dr-lim-meet.jpg" type="image/jpg" />
                    <img
                      className=""
                      src="/img/team/dr-lim-meet.jpg"
                      alt={configContext[0].doctors[0].fullName}
                    />
                  </picture>
                </FadeIn>
              </div>
              <div className="col-md-8 vcenter meet__doctor--text">
                <h2 className="meet__title">Meet</h2>
                <SlideLeft>
                  <div className="animation-element slide-left">
                    <h3 className="meet__doctor--name ">
                      {configContext[0].doctors[0].firstName} {configContext[0].doctors[0].lastName}
                      , DDS
                    </h3>
                    <p>
                      {configContext[0].doctors[0].fullName} has been rehabilitating patients'
                      mouths since 2000. Dr. Lim cares for patients with jaw joint problems, jaw
                      pains, headaches and bite problems. The best solution to many of these
                      symptoms is often the preservation of what your body was “built” to do. This
                      is often dictated by how your jaw closes and the joint position.
                    </p>
                  </div>
                </SlideLeft>
              </div>
            </div>
          </div>
        </div>
        <Link href="/our-doctor">
          <a className="meet__link">
            {' '}
            Meet Our Doctor <span className="ml-4">⟶</span>
          </a>
        </Link>
      </section>
    </>
  );
}

export default HomeMeet;
