import { useContext } from 'react';
import { Row, Container } from 'react-bootstrap';

import ModalContext from '@/contexts/ModalContext';
import { SmallDoctor } from '@/components/Common';
import { ModalWrapper, Content, Title, Close, Aside, StyledLogo } from './styles';

const Modal = () => {
  const { open, handleClose } = useContext(ModalContext);

  return (
    <ModalWrapper show={open} onHide={handleClose}>
      <Container fluid>
        <Row>
          <Content>
            <Title />
          </Content>
          <Aside>
            <Close onClick={handleClose}>
              <span>✕</span>
            </Close>
            <StyledLogo />
            <SmallDoctor />
          </Aside>
        </Row>
      </Container>
    </ModalWrapper>
  );
};

export default Modal;
