import styled from 'styled-components';
import { Modal as BModal, Button } from 'react-bootstrap';
import { Logo } from '@/components/Common/';

export const ModalWrapper = styled(BModal)`
  .modal-content {
    border-radius: 15px;
    overflow: hidden;
  }
  .modal-dialog {
    max-width: 950px;
  }
`;

export const Close = styled(Button)`
  float: right;
  font-size: 24px;
  font-weight: 800;
  line-height: 1;
  color: ${props => props.theme.colors.darkmain};
  opacity: 1;
  cursor: pointer;
  padding: 0;
  background-color: transparent;
  border: 0;
  &:hover {
    opacity: 0.5;
    background-color: transparent;
    color: ${props => props.theme.colors.darkmain};
  }
`;

export const Title = styled(BModal.Title)`
  margin: 0 0 35px 0;
  color: ${props => props.theme.colors.darkmain};
  font-size: 25px;
  line-height: 30px;
  font-weight: normal;
`;

export const Content = styled.div`
  padding: 45px;
  width: calc(100% - 340px);
  @media (${props => props.theme.breakpoint.down.sm}) {
    width: 100%;
  }
`;

export const Aside = styled.div`
  background-color: ${props => props.theme.colors.lightwedo};
  padding: 35px;
  width: 340px;
  @media (${props => props.theme.breakpoint.down.sm}) {
    display: none;
  }
`;

export const StyledLogo = styled(Logo)`
  max-width: 240px;
  margin-right: auto;
  margin-left: auto;
  display: block;
`;
