import { useContext } from 'react';
import ConfigContext from '@/contexts/ConfigContext';

import { ButtonSchedule, Logo } from './Common';

function Header() {
  const { configContext } = useContext(ConfigContext);
  return (
    <>
      <div className="hero__logo">
        <div className="container hero__logoinner">
          <div className="row">
            <div className="col">
              <Logo className="header__logo" />
            </div>
            <div className="col" />
            <div className="col header__address">
              <div className="header__addressinner">
                <span className="icon icon-map-pin header__pin" />
                <a
                  className="location"
                  href={configContext[0].office.address.mapLink}
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  {`${configContext[0].office.address.street} ${
                    configContext[0].office.address.city
                  }`}
                  <span className="address-coma">,</span>{' '}
                  {`${configContext[0].office.address.state} ${
                    configContext[0].office.address.zip
                  }`}
                </a>
              </div>
              <ButtonSchedule>Schedule Your Visit</ButtonSchedule>
            </div>
          </div>
        </div>
      </div>
      <div className="header-spacer" />
    </>
  );
}

export default Header;
