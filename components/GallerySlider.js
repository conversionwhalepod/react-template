const GallerySlider = () => (
  <>
    <div className="office-tour-section">
      <div className="container">
        <h2 className="office-tour-section__title animation-element bounce-up">
          Take a Look Inside Our Office
        </h2>

        <div id="carousel-officetour">
          <div className="office-tour__slider">
            <div className="office-tour__sliderinner">
              <div className="office-tour__slide">
                <picture>
                  <source
                    srcSet="/img/office/oak-park-dental-associates-01.webp"
                    type="image/webp"
                  />
                  <source
                    srcSet="/img/office/oak-park-dental-associates-01.jpg"
                    type="image/jpg"
                  />
                  <img
                    className="d-block mw-100 m-auto"
                    src="/img/office/oak-park-dental-associates-01.jpg"
                    alt="REPLACE ME - Office"
                  />
                </picture>
              </div>

              <div className="office-tour__slide">
                <picture>
                  <source
                    srcSet="/img/office/oak-park-dental-associates-02.webp"
                    type="image/webp"
                  />
                  <source
                    srcSet="/img/office/oak-park-dental-associates-02.jpg"
                    type="image/jpg"
                  />
                  <img
                    className="d-block mw-100 m-auto"
                    src="/img/office/oak-park-dental-associates-02.jpg"
                    alt="REPLACE ME - Office"
                  />
                </picture>
              </div>

              <div className="office-tour__slide">
                <picture>
                  <source
                    srcSet="/img/office/oak-park-dental-associates-03.webp"
                    type="image/webp"
                  />
                  <source
                    srcSet="/img/office/oak-park-dental-associates-03.jpg"
                    type="image/jpg"
                  />
                  <img
                    className="d-block mw-100 m-auto"
                    src="/img/office/oak-park-dental-associates-03.jpg"
                    alt="REPLACE ME - Office"
                  />
                </picture>
              </div>

              <div className="office-tour__slide">
                <picture>
                  <source
                    srcSet="/img/office/oak-park-dental-associates-04.webp"
                    type="image/webp"
                  />
                  <source
                    srcSet="/img/office/oak-park-dental-associates-04.jpg"
                    type="image/jpg"
                  />
                  <img
                    className="d-block mw-100 m-auto"
                    src="/img/office/oak-park-dental-associates-04.jpg"
                    alt="REPLACE ME - Office"
                  />
                </picture>
              </div>

              <div className="office-tour__slide">
                <picture>
                  <source
                    srcSet="/img/office/oak-park-dental-associates-05.webp"
                    type="image/webp"
                  />
                  <source
                    srcSet="/img/office/oak-park-dental-associates-05.jpg"
                    type="image/jpg"
                  />
                  <img
                    className="d-block mw-100 m-auto"
                    src="/img/office/oak-park-dental-associates-05.jpg"
                    alt="REPLACE ME - Office"
                  />
                </picture>
              </div>

              <div className="office-tour__slide">
                <picture>
                  <source
                    srcSet="/img/office/oak-park-dental-associates-06.webp"
                    type="image/webp"
                  />
                  <source
                    srcSet="/img/office/oak-park-dental-associates-06.jpg"
                    type="image/jpg"
                  />
                  <img
                    className="d-block mw-100 m-auto"
                    src="/img/office/oak-park-dental-associates-06.jpg"
                    alt="REPLACE ME - Office"
                  />
                </picture>
              </div>

              <div className="office-tour__slide">
                <picture>
                  <source
                    srcSet="/img/office/oak-park-dental-associates-07.webp"
                    type="image/webp"
                  />
                  <source
                    srcSet="/img/office/oak-park-dental-associates-07.jpg"
                    type="image/jpg"
                  />
                  <img
                    className="d-block mw-100 m-auto"
                    src="/img/office/oak-park-dental-associates-07.jpg"
                    alt="REPLACE ME - Office"
                  />
                </picture>
              </div>

              <div className="office-tour__slide">
                <picture>
                  <source
                    srcSet="/img/office/oak-park-dental-associates-08.webp"
                    type="image/webp"
                  />
                  <source
                    srcSet="/img/office/oak-park-dental-associates-08.jpg"
                    type="image/jpg"
                  />
                  <img
                    className="d-block mw-100 m-auto"
                    src="/img/office/oak-park-dental-associates-08.jpg"
                    alt="REPLACE ME - Office"
                  />
                </picture>
              </div>

              <div className="office-tour__slide">
                <picture>
                  <source
                    srcSet="/img/office/oak-park-dental-associates-09.webp"
                    type="image/webp"
                  />
                  <source
                    srcSet="/img/office/oak-park-dental-associates-09.jpg"
                    type="image/jpg"
                  />
                  <img
                    className="d-block mw-100 m-auto"
                    src="/img/office/oak-park-dental-associates-09.jpg"
                    alt=" REPLACE ME - Office"
                  />
                </picture>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </>
);

export default GallerySlider;
