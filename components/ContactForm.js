import { useContext } from 'react';
import { useForm } from 'react-hook-form';
import ConfigContext from '@/contexts/ConfigContext';
import { PhoneLink, Logo, SmallDoctor } from '@/components/Common';
import storeContact from '../utils/storeContact';
import mailgunConnect from '../utils/mailgunConnect';

function ContactForm() {
  const { register, handleSubmit, errors } = useForm();
  const onSubmit = async data => {
    await storeContact(data);
    mailgunConnect(data);
  };
  const { configContext } = useContext(ConfigContext);
  return (
    <>
      <div className="container mb50">
        <h1 className="centered-heading">Contact Us</h1>

        <div className="contact-modal">
          <div className="container-fluid">
            <div className="row">
              <div className="contact-modal__content">
                <h2 className="contact-modal__title">
                  <span className="icon icon-email" /> Contact Us
                </h2>
                <div
                  className="response_msg alert alert-success mt30"
                  id="error_message"
                  //   style={{ display: 'none' }}
                >
                  {errors.email && <p>Email Required</p>}{' '}
                  {errors.message && <p>Message Required</p>}
                </div>
                <form name="contact-form" id="contact-form" onSubmit={handleSubmit(onSubmit)}>
                  <input
                    hidden
                    type="text"
                    name="source"
                    value="Contact Modal"
                    readOnly
                    ref={register}
                  />
                  <div className="form-row">
                    <div className="col-lg-6 col-sm-12">
                      <label htmlFor="fullname">Full Name</label>
                      <input
                        name="fullname"
                        type="text"
                        className="form-control"
                        id="name"
                        placeholder=""
                        ref={register({ required: true })}
                      />
                    </div>
                    <div className="col-lg-6 col-sm-12">
                      <label htmlFor="cellphone">Enter Cell Phone</label>
                      <input
                        name="phone"
                        type="text"
                        id="cellphone"
                        className="form-control phone"
                        placeholder=""
                        ref={register}
                      />
                    </div>
                  </div>
                  <div className="form-row">
                    <div className="col-lg-12 col-sm-12">
                      <label htmlFor="email">Email (required)</label>
                      <input
                        name="email"
                        id="email"
                        type="email"
                        className="form-control"
                        placeholder=""
                        ref={register({ required: true })}
                      />
                    </div>
                  </div>

                  <div className="form-row">
                    <div className="col-lg-12">
                      <div className="form-group">
                        <label htmlFor="msg">Message (required)</label>
                        <textarea
                          className="form-control"
                          id="msg"
                          name="message"
                          rows="6"
                          placeholder=""
                          ref={register({ required: true })}
                        />
                      </div>
                    </div>
                  </div>

                  <div className="form-row">
                    <div className="col-lg-6 col-sm-12">
                      <div className="form-group">
                        <button type="submit" className="btn btn-submit contact-modal__submit">
                          Submit
                        </button>
                      </div>
                    </div>
                    <div className="col-lg-6 col-sm-12 contact-modal__call">
                      If you prefer to speak to a team member, please call
                      <PhoneLink number={false} />.
                    </div>
                  </div>
                </form>
              </div>

              <div className="contact-modal__aside">
                <Logo className="contact-modal__logo" />
                <SmallDoctor />
                <div className="contact-modal__reviews modal-review">
                  <div className="modal-review__slider">
                    <div className="modal-review__item">
                      <h3 className="modal-review__title">
                        {configContext[0].testimonials[0].name}
                      </h3>
                      <div>{configContext[0].testimonials[0].review}</div>
                      <div className="modal-review__rating">
                        <img src="[REPLACE ME - testimonial site]" alt="review site" />
                        <img src="/img/start-reviews.png" alt="five stars" />
                      </div>
                    </div>

                    <div className="modal-review__item">
                      <h3 className="modal-review__title">
                        {configContext[0].testimonials[1].name}
                      </h3>
                      <div>{configContext[0].testimonials[1].review}</div>
                      <div className="modal-review__rating">
                        <img src="[REPLACE ME - testimonial site]" alt="review site" />
                        <img src="/img/start-reviews.png" alt="five stars" />
                      </div>
                    </div>

                    <div className="modal-review__item">
                      <h3 className="modal-review__title">
                        {configContext[0].testimonials[2].name}]
                      </h3>
                      <div>{configContext[0].testimonials[2].review}</div>
                      <div className="modal-review__rating">
                        <img src="[REPLACE ME - testimonial site]" alt="review site" />
                        <img src="/img/start-reviews.png" alt="five stars" />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default ContactForm;
