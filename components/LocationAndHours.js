import Link from 'next/link';
import { useContext } from 'react';
import ConfigContext from '@/contexts/ConfigContext';

import { ButtonSchedule, GoogleMap, PhoneLink } from './Common';

function LocationAndHours() {
  const { configContext } = useContext(ConfigContext);
  return (
    <>
      <div className="office__wrapper">
        <h1 className="location__title">Location &amp; Hours</h1>
        <div className="office container">
          <div className="row">
            <div className="office__intro-copy">
              <div className="row">
                <div className="col-lg-4">
                  <h2>
                    Contact <br />
                    {configContext[0].office.office}
                  </h2>
                </div>
                <div className="col-lg-8">
                  <p>
                    We invite you to contact us anytime with questions, comments, requests or
                    suggestions. We will respond to you as soon as possible.
                  </p>
                </div>
              </div>
            </div>

            <div className="office__photo" id="photoswap">
              <a href="" className="btn btn-cta" tabIndex="0" style={{ display: 'none' }}>
                Book Appointment
              </a>
            </div>

            <div className="col-lg-12">
              <ul className="nav nav-tabs" id="locationTabs" role="tablist">
                <li className="nav-item">
                  <a
                    className="nav-link active"
                    data-toggle="tab"
                    href="#office1"
                    role="tab"
                    aria-controls="office1"
                    aria-selected="true"
                  >
                    {configContext[0].office.address.city}
                  </a>
                </li>
              </ul>
              <div className="tab-content" id="locationTabContent">
                <div className="row tab-wrap">
                  <div className="col-lg-6 tab-pane__info">
                    <div className="row">
                      <div className="col-6">
                        <h3>
                          {' '}
                          <span className="icon icon-gps" /> Address
                        </h3>
                        <a
                          className="location"
                          href={configContext[0].office.address.mapLink}
                          target="_blank"
                          rel="noopener noreferrer"
                        >
                          {`${configContext[0].office.address.street} ${
                            configContext[0].office.address.city
                          }`}
                          <span className="address-coma">,</span>{' '}
                          {`${configContext[0].office.address.state} ${
                            configContext[0].office.address.zip
                          }`}
                        </a>
                      </div>
                      <div className="col-6">
                        <h3>
                          {' '}
                          <span className="icon icon-call" /> Phone
                        </h3>
                        <PhoneLink number={false} />
                      </div>
                    </div>
                    <div className="table__wrapper">
                      <table className="table">
                        <thead>
                          <tr>
                            <th colSpan="2" scope="col">
                              Office Hours
                            </th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>Monday</td>
                            <td>{configContext[0].office.hours[1].Monday}</td>
                          </tr>
                          <tr>
                            <td>Tuesday</td>
                            <td>{configContext[0].office.hours[2].Tuesday}</td>
                          </tr>
                          <tr>
                            <td>Wednesday</td>
                            <td>{configContext[0].office.hours[3].Wednesday}</td>
                          </tr>
                          <tr>
                            <td>Thursday</td>
                            <td>{configContext[0].office.hours[4].Thursday}</td>
                          </tr>
                          <tr>
                            <td>Friday</td>
                            <td>{configContext[0].office.hours[5].Friday}</td>
                          </tr>
                          <tr>
                            <td>Saturday</td>
                            <td>{configContext[0].office.hours[6].Saturday}</td>
                          </tr>
                          <tr>
                            <td>Sunday</td>
                            <td>{configContext[0].office.hours[0].Sunday}</td>
                          </tr>
                          {configContext[0].office.hoursNotes ? (
                            <tr>
                              <td colSpan="2">
                                <span className="note">{configContext[0].office.hoursNotes}</span>
                              </td>
                            </tr>
                          ) : null}
                        </tbody>
                      </table>
                    </div>
                  </div>
                  <div className="col-lg-6 tab-pane__landmarks">
                    <div className="text-center">
                      <ButtonSchedule />
                    </div>
                    <GoogleMap />
                    <h2 className="mt-3">Directions</h2>
                    <p>{configContext[0].office.address.directions}</p>
                    <a href="<?php echo $map; ?>" target="_blank" className="btn btn-white">
                      Get Directions
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className="panel-group" id="accordion" role="tablist" aria-multiselectable="true" />
        </div>
      </div>
    </>
  );
}

export default LocationAndHours;
