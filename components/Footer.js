import Link from 'next/link';
import { useContext } from 'react';
import ConfigContext from '@/contexts/ConfigContext';

import { ButtonSchedule, PhoneLink, FooterSocial, Logo } from './Common';

function Footer({ links }) {
  const { configContext } = useContext(ConfigContext);
  const colOne = links[0].columns[0].columnTitle;
  const colTwo = links[0].columns[1].columnTitle;
  const colThree = links[0].columns[2].columnTitle;
  return (
    <>
      <footer className="footer__wrapper">
        <div className="footer container">
          <div className="row">
            <div className="col-xl-2 col-lg-2 col-6">
              <h3 className="footer-menu__title">{colOne}</h3>
              <ul className="footer-menu">
                {links[0].columns[0].columnLinks.map((el, index) => (
                  <li className="footer-menu__item" key={`${colOne}-${index}`}>
                    <Link href={el.link}>
                      <a className="footer-menu__link">{el.title}</a>
                    </Link>
                  </li>
                ))}
              </ul>
            </div>
            <div className="col-xl-2 col-lg-2 col-6">
              <h3 className="footer-menu__title">{colTwo}</h3>
              <ul className="footer-menu">
                {links[0].columns[1].columnLinks.map((el, index) => (
                  <li className="footer-menu__item" key={`${colTwo}-${index}`}>
                    <Link href={el.link}>
                      <a className="footer-menu__link">{el.title}</a>
                    </Link>
                  </li>
                ))}
              </ul>
            </div>
            <div className="col-xl-2 col-lg-2 col-6 footer__support">
              <h3 className="footer-menu__title">{colThree}</h3>
              <ul className="footer-menu">
                {links[0].columns[2].columnLinks.map((el, index) => (
                  <li className="footer-menu__item" key={`${colThree}-${index}`}>
                    <Link href={el.link}>
                      <a className="footer-menu__link">{el.title}</a>
                    </Link>
                  </li>
                ))}
              </ul>
            </div>
            <div className="col-xl-3 col-lg-3 col-6 footer__questions">
              <h3 className="footer__title">Have Questions?</h3>
              Call Us Now at:
              <br />
              <PhoneLink number={false} />
              <br />
              to schedule your visit!
              <ButtonSchedule size="sm" />
            </div>
            <div className="col-xl-3 col-lg-3 col-12">
              <FooterSocial
                facebook
                yelp
                twitter={false}
                instagram={false}
                youtube={false}
                linkedin={false}
                pinterest={false}
                feedburner={false}
              />
            </div>
          </div>

          <div className="row bottom-address">
            <div className="col-12">
              <h5>{configContext[0].office.office}</h5>
              <a
                className="location"
                href={configContext[0].office.address.mapLink}
                target="_blank"
                rel="noopener noreferrer"
              >
                {`${configContext[0].office.address.street}`}
                <br />{' '}
                {`${configContext[0].office.address.city}
              ${configContext[0].office.address.state} ${configContext[0].office.address.zip}`}
              </a>
              <br />
              <PhoneLink number={false} />
            </div>
          </div>

          <div className="row footer__copyright">
            <div className="col-xl-3 col-lg-3 col-12">
              <Logo className="footer__logo lazyload" variant="white" />
            </div>
            <div className="col-xl-6 col-lg-5 col-12 footer__center">
              <p>
                © {new Date().getFullYear()}
                {` `}
                {configContext[0].office.office}. All rights reserved.
                <br />
                <Link href="/privacy-policy">
                  <a>Privacy Policy</a>
                </Link>{' '}
                &nbsp;&nbsp;|&nbsp;&nbsp;
                <Link href="/accessibility">
                  <a>Accessibility</a>
                </Link>{' '}
                &nbsp;&nbsp;|&nbsp;&nbsp;
                <Link href="/disclaimer">
                  <a>Disclaimer</a>
                </Link>
              </p>
            </div>
            <div className="col-xl-3 col-lg-4 col-12 footer__right">
              <p>
                <a href="//conversionwhale.com/dental-marketing" target="_blank" rel="noopener">
                  Dental Marketing{' '}
                </a>
                by Conversion Whale
              </p>
            </div>
          </div>
        </div>
      </footer>
    </>
  );
}

export default Footer;
