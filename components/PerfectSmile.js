import { Row, Col } from 'react-bootstrap';

import { useContext } from 'react';
import ConfigContext from '@/contexts/ConfigContext';

import { ButtonSchedule, PhoneLink } from './Common';

function PerfectSmile() {
  const { configContext } = useContext(ConfigContext);
  return (
    <>
      <div className="help__wrapper text-center">
        <div className="help">
          <h2 className="help__title">
            <strong>Dentist</strong> Proudly Serving
            <br />
            <strong>{configContext[0].office.address.city}</strong> Since{' '}
            {configContext[0].office.sinceYear}
          </h2>
          <div className="row">
            <div className="col-12">
              <picture>
                <source srcSet="/img/city-seal.webp" type="image/webp" />
                <source srcSet="/img/city-seal.png" type="image/png" />
                <img
                  src="/img/city-seal.png"
                  style={{ maxWidth: '150px', height: 'auto' }}
                  alt="REPLACE ME - City Seal"
                  className="img-fluid d-block mx-auto lazyload"
                />
              </picture>
            </div>
          </div>
          <br />
          <ButtonSchedule>Schedule Your Visit</ButtonSchedule>

          <div className="schedule__questions mt-3">
            <p className="mb-0">
              Have Any Questions?
              <br /> Call Us Today at:{` `}
              <PhoneLink number={false} />
            </p>
          </div>
        </div>
      </div>
    </>
  );
}

export default PerfectSmile;
