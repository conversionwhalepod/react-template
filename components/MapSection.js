import { useContext } from 'react';
import ConfigContext from '@/contexts/ConfigContext';

import { ButtonSchedule, GoogleMap } from './Common';

function MapSection({ directions }) {
  const { configContext } = useContext(ConfigContext);
  return (
    <>
      <div className="location__wrapper">
        <div className="location container">
          <h2 className="location__title">
            Conveniently Located
            <small>
              in {configContext[0].office.address.city}, {configContext[0].office.address.fullState}
            </small>
          </h2>
          <div className="row">
            <div className="col-xl-7 col-md-6 col-lg-7">
              <GoogleMap boxShadow />
            </div>
            <div className="col-xl-5 col-md-6 col-lg-5 location__content">
              <h3 className="location__directions">
                Find My Dentist in {configContext[0].office.address.city},{' '}
                {configContext[0].office.address.fullState}
              </h3>
              <br />
              <h3 className="location__address text-center text-md-left">Address:</h3>
              <div className="location__text text-center text-md-left">
                <a
                  className="location"
                  href={configContext[0].office.address.mapLink}
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  {configContext[0].office.address.street}
                  <br />
                  {configContext[0].office.address.city}
                  <span className="address-coma">,</span> {configContext[0].office.address.state}{' '}
                  {configContext[0].office.address.zip}
                </a>
              </div>

              <div className="location__direct">
                <div className="location__text">
                  <h4>Directions</h4>
                  <p>{directions || configContext[0].office.address.directions}</p>
                </div>
              </div>
              <div className="text-center text-md-left">
                <ButtonSchedule variant="outline-white">Schedule Your Visit</ButtonSchedule>
              </div>
            </div>
          </div>
          <div className="location__serving text-center">
            PROUDLY SERVING
            <br />
            {configContext[0].office.address.city}
            <br />
            Since {configContext[0].office.sinceYear}
          </div>
          <div className="row mt-4">
            <div className="col-md-2 offset-md-5 col-4 offset-4 mb-4">
              <picture>
                <source srcSet="/img/city-seal.webp" type="image/webp" />
                <source srcSet="/img/city-seal.png" type="image/png" />
                <img
                  src="/img/city-seal.png"
                  alt="{configContext[0].office.address.city} Seal"
                  className="img-fluid d-block mx-auto lazyload"
                />
              </picture>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
export default MapSection;
