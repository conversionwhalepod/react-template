import PropTypes from 'prop-types';

import {
  TestimonialCol,
  TestimonialDiv,
  TestimonialBlockquote,
  TestimonialText,
  TestimonialFooter,
  TestimonialName
} from './styles';

export const TestimonalSingle = ({
  color,
  size,
  leftName,
  leftTestimonial,
  rightName,
  rightTestimonial
}) => (
  <>
    {leftName || rightName ? (
      <TestimonialCol md={size === 'small' ? 5 : 7}>
        <TestimonialDiv>
          <TestimonialBlockquote color={color}>
            <TestimonialText>{leftTestimonial || rightTestimonial}</TestimonialText>
          </TestimonialBlockquote>
        </TestimonialDiv>
        <TestimonialFooter>
          <TestimonialName>{leftName || rightName}</TestimonialName>
          <img src="/img/start-reviews.png" alt="Five stars" title="Five stars" />
        </TestimonialFooter>
      </TestimonialCol>
    ) : null}
  </>
);

TestimonalSingle.propTypes = {
  color: PropTypes.string,
  size: PropTypes.string,
  leftName: PropTypes.string,
  leftTestimonial: PropTypes.string,
  rightName: PropTypes.string,
  rightTestimonial: PropTypes.string
};
