import PropTypes from 'prop-types';
import Row from 'react-bootstrap/Row';
import { TestimonalSingle } from './Single';

export const Testimonial = ({
  position,
  leftName,
  leftTestimonial,
  rightName,
  rightTestimonial
}) => (
  <>
    <Row>
      {position === 'left' ? (
        <>
          <TestimonalSingle
            color="light"
            size="small"
            leftName={leftName}
            leftTestimonial={leftTestimonial}
          />
          <TestimonalSingle
            color="dark"
            size="large"
            rightName={rightName}
            rightTestimonial={rightTestimonial}
          />
        </>
      ) : (
        <>
          <TestimonalSingle
            color="dark"
            size="large"
            leftName={leftName}
            leftTestimonial={leftTestimonial}
          />
          <TestimonalSingle
            color="light"
            size="small"
            rightName={rightName}
            rightTestimonial={rightTestimonial}
          />
        </>
      )}
    </Row>
  </>
);

Testimonial.propTypes = {
  position: PropTypes.string,
  leftName: PropTypes.string,
  leftTestimonial: PropTypes.string,
  rightName: PropTypes.string,
  rightTestimonial: PropTypes.string
};

Testimonial.defaultProps = {
  position: 'left'
};
