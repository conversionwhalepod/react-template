import styled from 'styled-components';
import Col from 'react-bootstrap/Col';

export const TestimonialCol = styled(Col)`
  margin-bottom: 3rem !important;
`;

export const TestimonialDiv = styled.div`
  position: relative;
`;

export const TestimonialBlockquote = styled.blockquote`
  padding: 0px;
  border: 0;
  margin: 0;
  font-style: italic;
  border-radius: 8px;
  font-family: 'Prompt', sans-serif;
  font-weight: 300;
  font-size: 16px;
  color: #333;
  background: ${props =>
    props.color === 'light' ? props.theme.colors.lightmain : props.theme.colors.darkmain};
`;

export const TestimonialText = styled.p`
  color: #fff;
  padding-top: 25px;
  padding-bottom: 45px;
  padding-left: 30px;
  padding-right: 30px;
  margin-top: 0;
  margin-bottom: 1rem;
`;

export const TestimonialFooter = styled.div`
  margin: 10px 0;
  text-align: left !important;
`;

export const TestimonialName = styled.h4`
  font-size: 14px;
  margin-bottom: 4px;
`;
