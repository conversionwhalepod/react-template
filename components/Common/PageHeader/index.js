import { Row, Col } from 'react-bootstrap';

import {
  HeaderContainer,
  HeaderWrapper,
  FirstHeading,
  SecondHeading,
  Icon
} from './PageHeader';

export const PageHeader = ({ title, subTitle, icon }) => (
  <>
    <HeaderContainer>
      <HeaderWrapper>
        <Row>
          <Col>
            <FirstHeading className="mt-0">
              {icon && <Icon className={`icon-${icon}`} />}
              {title || `Page Title`}
            </FirstHeading>
            {subTitle && <SecondHeading>{subTitle}</SecondHeading>}
          </Col>
        </Row>
      </HeaderWrapper>
    </HeaderContainer>
  </>
);
