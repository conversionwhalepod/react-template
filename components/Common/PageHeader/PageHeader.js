import styled from 'styled-components';

export const HeaderContainer = styled.section`
  margin-bottom: 2.5%;
`;

export const HeaderWrapper = styled.div`
  border-bottom: 4px solid ${props => props.theme.colors.lightmain};
  background: ${props => props.theme.colors.darkmain}
    url('/img/teledentistry/teledentistry-hero.svg') no-repeat bottom left;
  background-size: cover;
  padding: 5% 10%;
  text-align: center;
  color: white;
  overflow: hidden;
  position: relative;
`;

export const FirstHeading = styled.h1`
  font-size: 3em;
  position: relative;
  z-index: 1;
  @media (${props => props.theme.breakpoint.down.lg}) {
    font-size: 2.5em;
  }
  @media (${props => props.theme.breakpoint.down.sm}) {
    font-size: 2em;
  }
`;

export const SecondHeading = styled.h2`
  display: inline-block;
  border-top: 2px solid ${props => props.theme.colors.lightmain};
  padding-top: 1em;
  color: ${props => props.theme.colors.lightmain};
  font-weight: 300;
  font-size: 1.5em;
  text-transform: none;
  margin-top: 1em;
`;

export const Icon = styled.span`
  color: ${props => props.theme.colors.lightmain};
  display: block;
  margin: auto;
  position: absolute;
  top: -0.1em;
  left: 0;
  font-size: 600%;
  z-index: -1;
  opacity: 0.1;
`;
