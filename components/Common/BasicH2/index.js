import styled from 'styled-components';

export const BasicH2 = styled.h2`
  font-size: 1.75em;
  font-weight: 400;
  color: #1f6c96;
  margin-top: 1em;
  margin-bottom: 0.5em;
  position: relative;
`;
