import { Container, Row, Col } from 'react-bootstrap';

export const BasicContent = ({ children }) => (
  <>
    <Container className="mb-5">
      <Row>
        <Col>{children}</Col>
      </Row>
    </Container>
  </>
);
