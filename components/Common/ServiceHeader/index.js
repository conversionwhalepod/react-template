import { useContext } from 'react';
import ConfigContext from '@/contexts/ConfigContext';
import {
  ServiceSection,
  HeroIcon,
  HeroTitle,
  HeroInCity,
  HeroDescription,
  HeroWrapper
} from './styles';
import { ButtonSchedule } from '../ButtonSchedule';

export function ServiceHeader({ background, icon, title, inCity, children }) {
  const { configContext } = useContext(ConfigContext);
  return (
    <>
      <ServiceSection variant={background}>
        <HeroWrapper>
          <HeroIcon className={`icon icon-${icon}`} />
          <HeroTitle>
            {title}
            <br />
            {inCity && (
              <HeroInCity>
                in {configContext[0].office.address.city}, {configContext[0].office.address.state}
              </HeroInCity>
            )}
          </HeroTitle>
          {children && <HeroDescription>{children}</HeroDescription>}
          <ButtonSchedule size="lg">
            <span className="icon icon-calendar" /> Book Now
          </ButtonSchedule>
        </HeroWrapper>
      </ServiceSection>
    </>
  );
}
