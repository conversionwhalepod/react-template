import styled, { css } from 'styled-components';

export const ServiceSection = styled.section`
  ::before {
    content: '';
    width: 100%;
    height: 100%;
    position: absolute;
    top: 0;
    left: 0;
    background: ${props => props.theme.colors.darkmain};
    opacity: 0.8;
  }
  background-size: cover;
  background-attachment: fixed;
  color: #fff;
  font-size: 1.2em;
  text-align: center;
  position: relative;
  ${props =>
    props.variant === 'crowns'
      ? css`
        background-image: url('/img/banner-crowns.jpg');
            }
          `
      : null}
`;

export const HeroDescription = styled.p`
  margin-top: 0;
  margin-bottom: 1rem;
`;

export const HeroWrapper = styled.div`
  position: relative;
  z-index: 1;
  top: 0;
  left: 0;
  padding: 4em 30%;
`;

export const HeroTitle = styled.h1`
  font-size: 2em;
`;

export const HeroInCity = styled.span`
  font-size: 0.6em;
  font-weight: 300;
`;

export const HeroIcon = styled.span`
  color: #fff;
  font-size: 4em;
`;
