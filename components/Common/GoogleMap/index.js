import { useContext } from 'react';
import PropTypes from 'prop-types';
import ConfigContext from '@/contexts/ConfigContext';

import { MapContainer, StyledIframe } from './styles';

export const GoogleMap = ({ iframeURL, boxShadow }) => {
  const { configContext } = useContext(ConfigContext);
  return (
    <MapContainer boxShadow={boxShadow}>
      <StyledIframe
        src={iframeURL || configContext[0].office.address.iframeURL}
        width="600"
        height="450"
        frameBorder="0"
        style={{ border: 0 }}
        allowFullScreen
        className="lazyload"
        title="Location Map"
      />
    </MapContainer>
  );
};

GoogleMap.propTypes = {
  iframeURL: PropTypes.string.isRequired,
  boxShadow: PropTypes.bool
};
