import styled, { css } from 'styled-components';

export const MapContainer = styled.div`
  margin: 5em auto;
  overflow: hidden;
  padding-bottom: 50%;
  position: relative;
  height: 0;
  ${props =>
    props.boxShadow &&
    css`
      margin: 0 auto;
      padding-bottom: 67%;
      border: 1rem solid white;
      background-color: #ffffff;
      box-shadow: 0 10px 20px rgba(0, 0, 0, 0.2);
    `}
  @media (${props => props.theme.breakpoint.down.md}) {
    margin: 1em auto;
  }
`;

export const StyledIframe = styled.iframe`
  left: 0;
  top: 0;
  height: 100%;
  width: 100%;
  position: absolute;
`;
