import { useContext } from 'react';
import ConfigContext from '@/contexts/ConfigContext';

export const PhoneLink = ({ number }) => {
  const { configContext } = useContext(ConfigContext);
  const phoneNumber = number || configContext[0].office.phone;
  return <a href={`tel:${phoneNumber}`}>{phoneNumber}</a>;
};

export const PhoneNumber = () => {
  const { configContext } = useContext(ConfigContext);
  return configContext[0].office.phone;
};
