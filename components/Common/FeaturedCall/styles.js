import styled from 'styled-components';
import { Col } from 'react-bootstrap';

export const CallSection = styled.section`
  background-color: #fff;
  background-position: bottom right;
  background-repeat: no-repeat;
  background-size: 300px;
  background-image: none;
`;

export const CallWraper = styled.div`
  padding: 4vw 0;
`;

export const CallText = styled.p`
  font-size: 1.1em;
  font-style: italic;
`;

export const ColCenter = styled(Col)`
  align-self: center;
  text-align: center;
`;
