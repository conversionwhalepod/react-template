import { Container, Col, Row } from 'react-bootstrap';
import { CallSection, CallWraper, CallText, ColCenter } from './styles';
import { ButtonCall } from '../ButtonCall';

export const FeaturedCall = () => (
  <CallSection>
    <CallWraper>
      <Container>
        <Row>
          <Col md={8}>
            <CallText>
              Do you want to restore your smile with a crown or bridge? If so, don’t hesitate to
              contact Oak Park Dental Associates today. We will make sure you receive the best
              dental care and treatment.
            </CallText>
          </Col>
          <ColCenter md={4}>
            <ButtonCall size="lg" />
          </ColCenter>
        </Row>
      </Container>
    </CallWraper>
  </CallSection>
);
