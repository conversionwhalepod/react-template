import { useContext } from 'react';
import ConfigContext from '@/contexts/ConfigContext';

import { Button } from '../Button';

export const ButtonCall = ({ children, variant, size }) => {
  const { configContext } = useContext(ConfigContext);

  return (
    <Button href={`tel:${configContext[0].office.phone}`} variant={variant} size={size}>
      <span className="icon icon-phone" /> {children || configContext[0].office.phone}
    </Button>
  );
};
