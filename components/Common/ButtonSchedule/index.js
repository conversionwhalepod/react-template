import { useContext } from 'react';

import ConfigContext from '@/contexts/ConfigContext';
import ModalContext from '@/contexts/ModalContext';

import { Button } from '../Button';

// Add Url from localmed provided by doctor
const localMedUrl = 'https://www.localmed.com/widgets/c7598797-7a48-45ee-b0a5-8ae9ad4b109b/';

export const ButtonSchedule = ({ children, variant, size }) => {
  const { configContext } = useContext(ConfigContext);
  const { handleOpen } = useContext(ModalContext);
  const {
    office: { bookingMethod }
  } = configContext[0];

  return (
    <Button href="#" variant={variant} size={size} onClick={handleOpen}>
      {children || 'Book Appointment'}
    </Button>
  );
};
