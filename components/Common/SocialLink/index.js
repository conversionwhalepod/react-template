import { useContext } from 'react';
import ConfigContext from '@/contexts/ConfigContext';

export const SocialLink = ({ socialSite }) => {
  const { configContext } = useContext(ConfigContext);
  let socialURL;
  let socialLabel;
  let socialIcon;
  switch (socialSite) {
    case 'facebook':
      socialURL = configContext[0].social.facebook
        ? configContext[0].social.facebook
        : 'https://www.facebook.com/';
      socialLabel = 'Facebook';
      socialIcon = 'icon icon-facebook';
      break;
    case 'twitter':
      socialURL = configContext[0].social.twitter
        ? configContext[0].social.twitter
        : 'https://twitter.com/';
      socialLabel = 'Twitter';
      socialIcon = 'icon icon-twitter';
      break;
    case 'instagram':
      socialURL = configContext[0].social.instagram
        ? configContext[0].social.instagram
        : 'https://www.instagram.com/';
      socialLabel = 'Instagram';
      socialIcon = 'icon icon-insta';
      break;
    case 'youtube':
      socialURL = configContext[0].social.youtube
        ? configContext[0].social.youtube
        : 'https://www.youtube.com/';
      socialLabel = 'youtube';
      socialIcon = 'icon icon-youtube';
      break;
    case 'yelp':
      socialURL = configContext[0].social.yelpreview
        ? configContext[0].social.yelpreview
        : 'https://www.yelp.com/';
      socialLabel = 'yelp';
      socialIcon = 'icon icon-yelp';
      break;
    case 'linkedin':
      socialURL = configContext[0].social.linkedin
        ? configContext[0].social.linkedin
        : 'https://www.linkedin.com/';
      socialLabel = 'linkedin';
      socialIcon = 'icon icon-linkedin';
      break;
    case 'pinterest':
      socialURL = configContext[0].social.pinterest
        ? configContext[0].social.pinterest
        : 'https://www.pinterest.com/';
      socialLabel = 'pinterest';
      socialIcon = 'icon icon-pinterest';
      break;
    case 'feedburner':
      socialURL = configContext[0].social.feedburner
        ? configContext[0].social.feedburner
        : 'https://feedburner.google.com/';
      socialLabel = 'feedburner';
      socialIcon = 'icon icon-feedburner';
      break;
    default:
      socialURL = configContext[0].social.facebook
        ? configContext[0].social.facebook
        : 'https://www.facebook.com/';
      socialLabel = 'Facebook';
      socialIcon = 'icon icon-facebook';
  }
  return (
    <>
      <a
        className="footer__link"
        href={socialURL}
        target="_blank"
        aria-label={socialLabel}
        rel="noopener noreferrer"
      >
        <span className={socialIcon} />
      </a>
    </>
  );
};
