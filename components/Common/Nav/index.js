import styled, { css } from 'styled-components';

export const Nav = styled.div`
  position: fixed;
  @media (${props => props.theme.breakpoint.down.lg}) {
    box-shadow: 0px -5px 18px 0px rgba(87, 127, 155, 1);
  }
  top: 0;
  right: 0;
  left: 0;
  z-index: 1030;
`;

export const NavMenu = styled.div`
  flex-direction: column;
  z-index: 5;
  background-color: #fff;
  ::after {
    ${props =>
      props.scroll >= 123
        ? css`
            content: '';
            display: block;
            @media (${props.theme.breakpoint.up.xl}) {
              height: 1px;
              width: 100%;
              background: ${props.theme.colors.lightwedo};
            }
          `
        : null}
  }
`;

export const NavMenuContainer = styled.div`
  @media (${props => props.theme.breakpoint.down.lg}) {
    min-width: 100%;
  }
  max-width: none;
  padding-left: 3rem;
  padding-right: 5rem;
  @media (${props => props.theme.breakpoint.up.xl}) {
    padding-left: 5%;
    padding-right: 6.5%;
  }
  @media (${props => props.theme.breakpoint.up.xxxl}) {
    //max-width: 1100px;
    padding-left: 7.5%;
    padding-right: 5%;
  }
  @media (${props => props.theme.breakpoint.down.sm}) {
    padding-left: 0;
    padding-right: 0;
  }
  ::after {
    ${props =>
      props.scroll >= 123
        ? css`
            content: '';
            display: block;
            @media (${props.theme.breakpoint.up.xl}) {
              height: 1px;
              width: 100%;
              background: ${props.theme.colors.lightwedo};
            }
          `
        : null}
  }
`;

export const NavSub = styled.div`
  display: block;
  z-index: -2;
  background-color: #ffffff;
  height: 66px;
  position: absolute;
  top: -10px;
  width: 100%;
  transition: top 0.7s;
  transition-timing-function: ease-in-out;
  ${props =>
    props.scroll >= 123
      ? css`
          top: 81px;
          @media (${props.theme.breakpoint.up.xl}) {
            top: 55px;
          }
          @media (${props.theme.breakpoint.down.xl}) {
            display: none;
          }
        `
      : null}
  ::after {
    content: '';
    display: block;
    height: 4px;
    width: 100%;
    background-image: linear-gradient(
      to right,
      ${props => props.theme.colors.darkmain},
      ${props => props.theme.colors.lightmain},
      40%,
      ${props => props.theme.colors.mediummain}
    );
    background-repeat: no-repeat;
  }
`;

export const NavSubReviews = styled.div`
  padding-top: 18px;
  line-height: 1;
  font-size: 12px;
  color: #333;
`;

export const NavSubRating = styled.p`
  margin: 0;
  margin-top: 6px;
  padding: 0;
`;

export const NavSubCenter = styled.div`
  text-align: center;
  line-height: 65px;
`;

export const NavSubLast = styled.div`
  text-align: right;
`;
