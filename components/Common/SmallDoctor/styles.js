import styled from 'styled-components';

export const Button = styled.div`
  min-height: 90px;
  @media (${props => props.theme.breakpoint.down.lg}) {
    min-height: 60px;
  }
  &::after,
  &::before {
    position: absolute;
    left: 0;
    right: 65px;
    border-radius: 10px;
  }
  &::after {
    content: 'Book an \a  Appointment';
    top: 12px;
    bottom: 8px;
    background: ${props => props.theme.gradients.primary};
    color: white;
    font-weight: 600;
    padding: 21px 59px 0px 17px;
    line-height: 1.3;
  }
  &::before {
    content: '';
    background: ${props => props.theme.colors.darkmain};
    top: 50px;
    bottom: 0;
    left: 15px;
    transform: skewY(-4deg);
  }
  picture {
    display: block;
  }
  img {
    position: relative;
    z-index: 10;
    background: ${props => props.theme.colors.darkmain};
    width: 102px;
    border: 2px solid ${props => props.theme.colors.darkmain};
    border-radius: 50%;
    float: right;
    margin-top: -10px;
  }
`;

export const StyledLink = styled.a`
  background-image: url('/img/book-button.svg');
  display: block;
  background-repeat: no-repeat;
  background-position: left 0px bottom;
  background-size: 68%;
  overflow: hidden;
  padding-top: 20px;
  @media (${props => props.theme.breakpoint.down.sm}) {
    width: 240px;
    padding-top: 0px;
    margin-bottom: 12px;
  }
`;

export const Text = styled.div`
  color: ${props => props.theme.colors.darkmain};
  font-size: 13px;
  font-weight: bold;
  line-height: 16px;
  padding: 15px 15px 0 50px;
  margin-top: 0px;
  position: relative;
  &:before {
    content: '';
    display: block;
    width: 35px;
    height: 35px;
    background-image: url('/img/book-arrow.svg');
    background-repeat: no-repeat;
    background-position: top center;
    position: absolute;
    left: 10px;
    top: 15px;
  }
`;

export const Wrapper = styled.div`
  position: relative;
  margin-top: 10px;
`;
