import Link from 'next/link';
import { Button, StyledLink, Text, Wrapper } from './styles';

export const SmallDoctor = () => {
  return (
    <div className="mt-0">
      <Wrapper>
        <Link href="/" passHref>
          <StyledLink>
            <Button>
              <picture>
                <source srcSet="/img/team/dr-lim-icon.webp" type="image/webp" />
                <source srcSet="/img/team/dr-lim-icon.png" type="image/png" />
                <img src="/img/team/dr-lim-icon.png" alt="" />
              </picture>
            </Button>
          </StyledLink>
        </Link>
      </Wrapper>
      <Text>[REPLACE ME - small doctor info]</Text>
    </div>
  );
};
