import { useContext } from 'react';
import ConfigContext from '@/contexts/ConfigContext';
import { SocialLink } from '../SocialLink';

export const FooterSocial = ({
  facebook,
  twitter,
  instagram,
  youtube,
  yelp,
  linkedin,
  pinterest,
  feedburner
}) => {
  const { configContext } = useContext(ConfigContext);
  return (
    <>
      <div className="footer__social">
        {facebook && <SocialLink socialSite="facebook" />}
        {twitter && <SocialLink socialSite="twitter" />}
        {instagram && <SocialLink socialSite="instagram" />}
        {youtube && <SocialLink socialSite="youtube" />}
        {yelp && <SocialLink socialSite="yelp" />}
        {linkedin && <SocialLink socialSite="linkedin" />}
        {pinterest && <SocialLink socialSite="pinterest" />}
        {feedburner && <SocialLink socialSite="feedburner" />}
      </div>

      <div className="text-center text-lg-left mb-3">
        <a
          href={configContext[0].social.googlereview}
          target="_blank"
          rel="noopener noreferrer"
          aria-label={`Google Reviews For ${configContext[0].office.office}`}
        >
          <img
            className="footer__googlereviews"
            src="/img/google-reviews.svg"
            alt="Google Reviews *****"
          />
        </a>
      </div>
    </>
  );
};
