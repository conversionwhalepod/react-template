import styled, { css } from 'styled-components';

export const Button = styled.a`
  background-color: ${props => props.theme.colors.darkcta};
  background-image: ${props =>
    `linear-gradient(to right, ${props.theme.colors.darkcta} 0%, ${
      props.theme.colors.lightcta
    } 100%)`};
  border: 0;
  color: #fff;
  font-size: 0.9em;
  padding: 10px 33px;
  border-radius: 0.2rem;
  font-weight: 500;
  display: inline-block;
  &:hover {
    background-color: ${props => props.theme.colors.darkcta};
    background-image: none;
    color: #fff;
    text-decoration: none;
  }

  ${props =>
    props.variant === 'hero'
      ? css`
          text-align: left;
          display: block;
          max-width: 20em;
          margin: 0 auto 1em;
          position: relative;
          width: 100%;
          &:after {
            content: '⟶';
            position: absolute;
            right: calc(0% + 1em);
          }
        `
      : null}

  ${props =>
    props.variant === 'outline-white'
      ? css`
          background-color: #fff;
          background-image: none;
          color: ${props.theme.colors.darkmain};
          border: 1px solid;
          &:hover {
            background-color: ${props.theme.colors.darkmain};
            color: #fff;
          }
        `
      : null}

   ${props =>
     props.size === 'lg'
       ? css`
           font-size: 18px;
         `
       : null}

  ${props =>
    props.size === 'sm'
      ? css`
          font-size: 11px;
          text-transform: uppercase;
        `
      : null}

`;
