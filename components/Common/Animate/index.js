import React from 'react';
import handleViewport from 'react-in-viewport';
import styled, { css } from 'styled-components';

function viewport(props) {
  const { forwardedRef, className } = props;
  const childrenWithExtraProp = React.Children.map(props.children, child =>
    React.cloneElement(child, {
      ref: forwardedRef,
      className
    })
  );
  return childrenWithExtraProp;
}

const SlideLeftStyle = styled(viewport)`
  opacity: 0;
  position: relative;
  -moz-transition: all 700ms ease-out;
  -webkit-transition: all 700ms ease-out;
  -o-transition: all 700ms ease-out;
  transition: all 700ms ease-out;
  -moz-transform: translate3d(-100px, 0px, 0px);
  -webkit-transform: translate3d(-100px, 0px, 0px);
  -o-transform: translate(-100px, 0px);
  -ms-transform: translate(-100px, 0px);
  transform: translate3d(-100px, 0px, 0px);
  ${props =>
    props.inViewport
      ? css`
          opacity: 0.8;
          -moz-transform: scale(1);
          -webkit-transform: scale(1);
          -o-transform: scale(1);
          -ms-transform: scale(1);
          transform: scale(1);
        `
      : null}
`;

const SlideRightStyle = styled(viewport)`
  opacity: 0;
  position: relative;
  -moz-transition: all 700ms ease-out;
  -webkit-transition: all 700ms ease-out;
  -o-transition: all 700ms ease-out;
  transition: all 700ms ease-out;
  -moz-transform: translate3d(100px, 0px, 0px);
  -webkit-transform: translate3d(100px, 0px, 0px);
  -o-transform: translate(100px, 0px);
  -ms-transform: translate(100px, 0px);
  transform: translate3d(100px, 0px, 0px);
  ${props =>
    props.inViewport
      ? css`
          opacity: 0.8;
          -moz-transform: scale(1);
          -webkit-transform: scale(1);
          -o-transform: scale(1);
          -ms-transform: scale(1);
          transform: scale(1);
        `
      : null}
`;

const FadeInStyle = styled(viewport)`
  opacity: 0;
  position: relative;
  -moz-transition: all 1500ms ease-out;
  -webkit-transition: all 1500ms ease-out;
  -o-transition: all 1500ms ease-out;
  transition: all 1500ms ease-out;
  ${props =>
    props.inViewport
      ? css`
          opacity: 1;
        `
      : null}
`;

export const SlideLeft = handleViewport(SlideLeftStyle);

export const SlideRight = handleViewport(SlideRightStyle);

export const FadeIn = handleViewport(FadeInStyle);
