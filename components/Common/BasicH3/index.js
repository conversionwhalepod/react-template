import styled from 'styled-components';

export const BasicH3 = styled.h3`
  font-size: 1.5em;
  color: #5193b9;
  margin-top: 1.2em;
  margin-bottom: 0.5em;
  font-weight: 400;
`;
