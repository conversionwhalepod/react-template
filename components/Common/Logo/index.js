import { useContext } from 'react';
import Link from 'next/link';

import ConfigContext from '@/contexts/ConfigContext';

export const Logo = ({ variant, className }) => {
  const { configContext } = useContext(ConfigContext);
  const logoUrl =
    variant === 'white'
      ? '/img/oak-park-dental-associates-logo-white'
      : '/img/oak-park-dental-associates-logo';

  return (
    <Link href="/">
      <a aria-label="REPLACE ME - Office Logo">
        <picture>
          <source srcSet={`${logoUrl}.webp`} type="image/webp" />
          <source srcSet={`${logoUrl}.png`} type="image/png" />
          <img
            src={`${logoUrl}.png`}
            className={className}
            alt={`${configContext[0].office.office} Logo`}
          />
        </picture>
      </a>
    </Link>
  );
};
