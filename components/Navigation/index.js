import { useState, useEffect } from 'react';
import Link from 'next/link';
import { Nav, Row } from 'react-bootstrap';
import { headerNav } from '../../content';
import { Menu, StyledNavbar, NavLink, StyledDropdown, StyledContainer } from './styles';

import {
  ButtonSchedule,
  NavMenuContainer,
  NavSub,
  NavSubReviews,
  NavSubRating,
  NavSubCenter,
  NavSubLast,
  Logo
} from '../Common';

const Navigation = () => {
  const [scrollY, setScrollY] = useState(0);

  function logit() {
    setScrollY(window.pageYOffset);
  }

  useEffect(() => {
    function watchScroll() {
      window.addEventListener('scroll', logit);
    }
    watchScroll();
    return () => {
      window.removeEventListener('scroll', logit);
    };
  });

  return (
    <div className="fixed-top header sticky">
      <Menu>
        <StyledContainer>
          <StyledNavbar expand="xl">
            <StyledNavbar.Collapse>
              <Nav className="mr-auto" as="ul">
                {headerNav.map(link => {
                  if (link.children) {
                    return (
                      <StyledDropdown title={link.title} items={link.children} key={link.title} />
                    );
                  }
                  return (
                    <Nav.Item as="li" key={link.title}>
                      <Link href={link.path}>
                        <NavLink role="navigation">{link.title}</NavLink>
                      </Link>
                    </Nav.Item>
                  );
                })}
              </Nav>
            </StyledNavbar.Collapse>
          </StyledNavbar>
        </StyledContainer>
      </Menu>

      <NavSub scroll={scrollY}>
        <NavMenuContainer className="container">
          <Row>
            <NavSubReviews className="col-5">
              <span className="icon icon-map-pin inject" />
              <NavSubRating>
                <span>★</span>
                <span>★</span>
                <span>★</span>
                <span>★</span>
                <span>★</span> <span>400 + Google Reviews </span>
              </NavSubRating>
            </NavSubReviews>

            <NavSubCenter className="col-2">
              <Logo className="header__logosm" />
            </NavSubCenter>
            <NavSubLast className="col-5 pt-3">
              <ButtonSchedule />
            </NavSubLast>
          </Row>
        </NavMenuContainer>
      </NavSub>
    </div>
  );
};

export default Navigation;
