import Link from 'next/link';
import styled, { css } from 'styled-components';
import { Nav, Navbar, Dropdown, NavDropdown, Container } from 'react-bootstrap';
import PropTypes from 'prop-types';

export const Menu = styled.div`
  flex-direction: column;
  z-index: 5;
  background-color: #fff;
  &&& {
    .nav-item {
      &:first-child {
        .nav-link {
          padding-left: 0;
        }
      }
      &.dropdown.show {
        .nav-link {
          color: ${props => props.theme.colors.darkmain};
        }
      }
    }
    .dropdown-toggle {
      color: #333;
      padding-left: 16px;
      padding-right: 16px;
      &::after {
        border-style: solid;
        border-width: 2px 2px 0 0;
        border-color: ${props => props.theme.colors.darkmain};
        content: ' ';
        display: inline-block;
        height: 9px;
        left: 0px;
        position: relative;
        vertical-align: top;
        width: 9px;
        top: 5px;
        transform: rotate(135deg);
        @include media-breakpoint-down(lg) {
          border-color: #ffffff;
          position: absolute;
          top: 13px;
          right: 16px;
          left: auto;
        }
      }
    }
  }
`;

export const StyledContainer = styled(Container)`
  padding-left: 3rem;
  padding-right: 5rem;
  @media (${props => props.theme.breakpoint.up.xl}) {
    padding-left: 5%;
    padding-right: 6.5%;
    max-width: 100%;
  }
  @media (${props => props.theme.breakpoint.up.xxxl}) {
    padding-left: 7.5%;
    padding-right: 5%;
    max-width: 100%;
  }
  @media (${props => props.theme.breakpoint.down.sm}) {
    padding-left: 0;
    padding-right: 0;
    max-width: 100%;
  }
`;

export const NavLink = styled(Nav.Link)`
  &&& {
    color: ${props => props.theme.colors.secmain};
    padding-left: 16px;
    padding-right: 16px;
  }
`;

export const StyledDropdownMenu = styled(Dropdown.Menu)`
  top: 48px;
  column-count: 3;
  padding-bottom: 0;
  z-index: -1;
  min-width: 210px;
  padding: 0 0 10px 0;
  margin: 0;
  border-radius: 0;
  border: 0;
  border-left: 1px solid ${props => props.theme.colors.lightmain};
  border-right: 1px solid ${props => props.theme.colors.lightmain};
  box-shadow: 0px 5px 18px 0px rgba(87, 127, 155, 1);

  &:after {
    bottom: 0;
    left: 0;
    position: absolute;
    content: '';
    display: block;
    height: 4px;
    width: 100%;
    clear: left;
    ${({ theme: { colors } }) => css`
      background-image: linear-gradient(
        to right,
        ${colors.darkmain},
        ${colors.lightmain} 40%,
        ${colors.mediummain}
      );
    `}
  }
`;

export const StyledDropDownItem = styled(NavDropdown.Item)`
  font-size: 14px;
  font-weight: normal;
  color: ${props => props.theme.colors.darkmain};
  line-height: 37px;
  min-width: 200px;
  display: inline-block;
  border-bottom: 1px solid #e2edf3;
  &:hover {
    background-color: ${props => props.theme.colors.lightwedo};
    color: ${props => props.theme.colors.darkmain};
  }
`;

export const StyledNavbar = styled(Navbar)`
  padding: 0.5rem 0;
`;

export const StyledDropdown = ({ items, title }) => {
  return (
    <Dropdown as="li" className="nav-item">
      <Dropdown.Toggle
        className="nav-link"
        as="a"
        role="button"
        data-toggle="dropdown"
        id="navbarDropdown"
        href="#"
      >
        {title}
      </Dropdown.Toggle>
      <StyledDropdownMenu className="dropdown-menu-three-columns">
        {items.map(item => {
          return (
            <Link href={item.path} key={item.title} passHref>
              <StyledDropDownItem>
                {item.icon && <span className={`icon icon-${item.icon}`} />}
                {` ${item.title}`}
              </StyledDropDownItem>
            </Link>
          );
        })}
      </StyledDropdownMenu>
    </Dropdown>
  );
};

StyledDropdown.propTypes = {
  items: PropTypes.array.isRequired,
  title: PropTypes.string.isRequired
};
