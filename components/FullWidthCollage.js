import { useContext } from 'react';
import ConfigContext from '@/contexts/ConfigContext';

function FullWidthCollage() {
  const { configContext } = useContext(ConfigContext);
  const rand = Math.floor(Math.random() * 3) + 1;
  return (
    <>
      <div className="footer-image">
        <picture>
          <source srcSet="/img/oak-park-dental-associates-bottom-collage.webp" type="image/webp" />
          <source srcSet="/img/oak-park-dental-associates-bottom-collage.jpg" type="image/jpg" />
          <img
            src="/img/oak-park-dental-associates-bottom-collage.jpg"
            alt={`${configContext[0].office.office} Office Collage`}
            className="desktop"
          />
        </picture>

        <picture>
          <source
            srcSet={`/img/oak-park-dental-associates-mobile-hero-slide-${rand}.webp`}
            type="image/webp"
          />
          <source
            srcSet={`/img/oak-park-dental-associates-mobile-hero-slide-${rand}.jpg`}
            type="image/jpg"
          />
          <img
            src={`/img/oak-park-dental-associates-mobile-hero-slide-${rand}.webp`}
            alt={`${configContext[0].office.office} Office Collage`}
            className="mobile"
          />
        </picture>
      </div>
    </>
  );
}

export default FullWidthCollage;
