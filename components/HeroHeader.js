import { useContext } from 'react';
import ConfigContext from '@/contexts/ConfigContext';

import { ButtonSchedule, Logo } from './Common';

function HeroHeader() {
  const { configContext } = useContext(ConfigContext);
  return (
    <>
      <div className="home-hero__wrapper home-hero__wrapper--doctor home-hero__wrapper-- with-video">
        <div className="home-hero__address">
          <div className="header__addressinner">
            <span className="icon icon-map-pin header__pin" />
            <a
              className="location"
              href={configContext[0].office.address.mapLink}
              target="_blank"
              rel="noopener noreferrer"
            >
              {`${configContext[0].office.address.street} ${configContext[0].office.address.city}`}
              <span className="address-coma">,</span>{' '}
              {`${configContext[0].office.address.state} ${configContext[0].office.address.zip}`}
            </a>
          </div>
        </div>
        <div className="row home-hero__row no-gutters">
          <div className="col-md-4 col-xl-5 vcenter home-hero__company">
            <div className="home-hero__company--wrapper ">
              <Logo className="heroinner__logo" />
              <div>
                <ButtonSchedule variant="hero" size="lg" />
                <a
                  className="btn btn-cta btn-clear heroinner__cta"
                  href="#"
                  data-toggle="modal"
                  data-target="#home-hero__popup"
                  data-video="https://player.vimeo.com/video/414862926?autoplay=1&loop=1"
                >
                  Watch Our Video{' '}
                </a>
              </div>
            </div>
          </div>
          <div className="col-md-8 col-xl-7 vcenter home-hero__tagline">
            <div className="home-hero__tagline--wrapper">
              <div>
                <h2 className="home-hero__tagline-title home-hero__tagline-color">Smile!</h2>
                <h3 className="home-hero__tagline-subtitle">
                  You just found the{' '}
                  <span className="home-hero__tagline-color">
                    <strong>Top-Rated</strong>
                  </span>
                  <br /> Dentist in {configContext[0].office.address.city}.
                </h3>
              </div>
            </div>
          </div>
        </div>
        <div className="home-hero__doctor-container">
          <picture>
            <source srcSet="/img/doctor-photo.webp" type="image/webp" />
            <source srcSet="/img/doctor-photo.png" type="image/png" />
            <img src="/img/doctor-photo.png" alt="REPALCE ME - Doctor" />
          </picture>
        </div>
        <div className="home-hero__video">
          <video className="hero-video" autoPlay muted loop>
            <source className="hero-source" src="/img/preview.mp4" type="video/mp4" />
          </video>
        </div>
      </div>

      <div className="members-hero row no-gutters">
        <div className="col-lg-4 col-xl-6 vcenter members-hero__text">
          <h1 className="mb-0">
            Your Dedicated Dentist in {configContext[0].office.address.city},{' '}
            {configContext[0].office.address.fullState}
          </h1>
        </div>
        <div className="col-lg-8 col-xl-6 vcenter members-hero__logos">
          <img src="/img/associations/associations-ada.svg" alt="ADA" />
          <img
            src="/img/associations/associations-academy-of-general-dentistry.svg"
            alt="Academy Of General Dentistry"
          />
          <img src="/img/associations/associations-isds.svg" alt="ISDS" />
        </div>
      </div>
    </>
  );
}

export default HeroHeader;
