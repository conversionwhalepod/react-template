import { useContext } from 'react';
import ConfigContext from '@/contexts/ConfigContext';

const BookingScript = () => {
  const { configContext } = useContext(ConfigContext);
  const {
    office: { bookingMethod }
  } = configContext[0];

  switch (bookingMethod) {
    case 'localmed':
      return <script id="localmed-wjs" src="https://www.localmed.com/assets/web/js/widget.js" />;
    case 'zocdoc':
      return (
        <script
          type="text/javascript"
          async=""
          src="https://offsiteschedule.zocdoc.com/plugin/embed"
        />
      );
    default:
      return null;
  }
};

export default BookingScript;
