export default {
  title: 'Dentist in Oak Park, IL | Oak Park Dental Associates',
  description:
    'Keeping the smiles of Oak Park healthy and beautiful is our goal at Oak Park Dental Associates. We provide families with general, cosmetic, restorative, and pediatric dentistry. Schedule an appointment today.',
  openGraph: {
    type: 'website',
    locale: 'en_US',
    title: 'Dentist in Oak Park, IL | Oak Park Dental Associates',
    description:
      'Keeping the smiles of Oak Park healthy and beautiful is our goal at Oak Park Dental Associates. We provide families with general, cosmetic, restorative, and pediatric dentistry. Schedule an appointment today.',
    images: [
      {
        url: '/img/og.jpg',
      },
    ],
    url: 'https://www.oakparkdentalassociates.com',
    site_name: 'Oak Park Dental Associates',
  },
  twitter: {
    handle: '@handle',
    site: '@site',
    cardType: 'summary_large_image',
  },
};
