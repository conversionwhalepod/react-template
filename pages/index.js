import { NextSeo } from 'next-seo';
import PerfectSmile from '@/components/PerfectSmile';
import MapSection from '@/components/MapSection';
import HomeMeet from '@/components/HomeMeet';

export default function Home() {
  const title = 'Dentist in Oak Park, IL | Oak Park Dental Associates';
  const SEO = {
    title,
    description:
      'Keeping the smiles of Oak Park healthy and beautiful is our goal at Oak Park Dental Associates. We provide families with general, cosmetic, restorative, and pediatric dentistry. Schedule an appointment today.',
    openGraph: {
      title
    }
  };
  return (
    <>
      <NextSeo {...SEO} />
      <HomeMeet />
      <MapSection />
      <PerfectSmile />
    </>
  );
}
