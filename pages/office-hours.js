import { useEffect } from 'react';
import { useRouter } from 'next/router';
import { NextSeo } from 'next-seo';
import FullWidthCollage from '@/components/FullWidthCollage';
import PerfectSmile from '@/components/PerfectSmile';
import ContactForm from '@/components/ContactForm';
import LocationAndHours from '@/components/LocationAndHours';

export default function OfficeHours() {
  const router = useRouter();
  useEffect(() => {
    document.body.classList.add('office-hours');
  });
  const title = 'Location & Hours | Oak Park Dental Associates';
  const description =
    'View office hours and location for Oak Park Dental Associates in Oak Park, IL. We invite you to contact us anytime with questions, comments, requests or suggestions.';
  const baseURL = process.env.VERCEL_URL ? process.env.VERCEL_URL : 'http://localhost:3000';
  const SEO = {
    title,
    description,
    canonical: `${baseURL}${router.pathname}`,
    openGraph: {
      title,
      description
    }
  };
  return (
    <>
      <NextSeo {...SEO} />
      <LocationAndHours />
      <PerfectSmile />
      <FullWidthCollage />
    </>
  );
}
