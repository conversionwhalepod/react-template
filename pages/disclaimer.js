import { useRouter } from 'next/router';
import { NextSeo } from 'next-seo';
import PerfectSmile from '@/components/PerfectSmile';
import { PageHeader, BasicContent } from '@/components/Common';

export default function ContactUs() {
  const router = useRouter();
  const title = 'General Terms & Conditions | Disclaimer';
  const description = '';
  const baseURL = process.env.VERCEL_URL ? process.env.VERCEL_URL : 'http://localhost:3000';
  const SEO = {
    title,
    description,
    canonical: `${baseURL}${router.pathname}`,
    openGraph: {
      title,
      description
    }
  };
  return (
    <>
      <NextSeo {...SEO} />
      <PageHeader title="Disclaimer" subTitle="General Terms and Conditions" icon={false} />
      <BasicContent>
        <p>
          By accessing or using this website (the “Site”), you agree to be bound by all of the
          terms, conditions and notices contained in this Site Use Agreement. These general terms
          and conditions in this Agreement are referred to as this “Agreement.” We reserve the right
          to revise this Agreement at any time. By continuing to use the Site after such revisions
          are made, you express your understanding and agreement to such revised terms.
        </p>
        <p>
          The information on this Site is provided for general information, is not intended to
          provide medical, dental or surgical advice, and should not be relied upon as a substitute
          for professional medical advice, diagnosis or treatment. No doctor/patient relationship is
          established by your use of this Site. No diagnosis or treatment is being provided. The
          information contained here should be used in consultation with a doctor [dentist] of your
          choice. No guarantees or warranties are made regarding any of the information contained
          within this Site.
        </p>

        <p>
          We may provide links on the Site to other websites that are not under our control. In
          general, any website that has an address (or URL) not containing our domain name is such a
          website. These links are provided for convenience or reference only and are not intended
          as an endorsement by us of the organization or individual operating the website or a
          warranty of any type regarding the website or the information on the website.
        </p>

        <p>
          This practice has made and will continue to make efforts to include accurate and current
          information on this Site. However, the materials in our Site are provided “as is” and to
          the fullest extent permissible pursuant to applicable law, this practice disclaims all
          warranties, express or implied, including, without limitation, implied warranties of
          merchantability and fitness for a particular purpose. We do not assume any responsibility
          or risk for your use of the Site.
        </p>

        <p>
          WE ASSUME NO LIABILITY FOR LOSS OR DAMAGE HOWSOEVER RESULTING FROM ANY USE OF OR RELIANCE
          UPON THE SITE OR ITS CONTENT. IN NO EVENT WILL WE BE LIABLE FOR ANY LOST PROFITS, LOST
          SAVINGS, LOST DATA, BUSINESS INTERRUPTION, OR OTHER DIRECT, INDIRECT, SPECIAL,
          CONSEQUENTIAL OR INCIDENTAL DAMAGES OR CLAIMS (WHETHER IN CONTRACT, IN TORT, OR OTHERWISE)
          ARISING OUT OF OR RELATING TO THE SITE, ITS CONTENT, THE USE THEREOF, OR THE INABILITY TO
          USE THE SITE, EVEN IF WE HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH LOSS OR DAMAGE.
        </p>
      </BasicContent>
      <PerfectSmile />
    </>
  );
}
