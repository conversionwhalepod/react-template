import { useRouter } from 'next/router';
import { NextSeo } from 'next-seo';
import PerfectSmile from '@/components/PerfectSmile';
import FullWidthCollage from '@/components/FullWidthCollage';
import ContactForm from '@/components/ContactForm';

export default function ContactUs() {
  const router = useRouter();
  const title = 'Contact Us | Oak Park Dental Associates';
  const description =
    'Contact Oak Park Dental Associates in Oak Park to schedule a free dental consultation, or for more information regarding an upcoming appointment. Contact by phone or submit by form online.';
  const baseURL = process.env.VERCEL_URL ? process.env.VERCEL_URL : 'http://localhost:3000';
  const SEO = {
    title,
    description,
    canonical: `${baseURL}${router.pathname}`,
    openGraph: {
      title,
      description
    }
  };
  return (
    <>
      <NextSeo {...SEO} />
      <FullWidthCollage />
      <ContactForm />
      <PerfectSmile />
    </>
  );
}
