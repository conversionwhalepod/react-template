import { useRouter } from 'next/router';
import { NextSeo } from 'next-seo';
import PerfectSmile from '@/components/PerfectSmile';
import { PageHeader, BasicContent, Testimonial } from '@/components/Common';
import FullWidthCollage from '@/components/FullWidthCollage';

export default function ContactUs() {
  const router = useRouter();
  const title = 'Dental Patient Testimonials';
  const description =
    'Read real patient reviews and testimonials of Oak Park Dental Associates in Oak Park. Oak Park Dental Associates proudly serves Oak Park and surrounding areas.';
  const baseURL = process.env.VERCEL_URL ? process.env.VERCEL_URL : 'http://localhost:3000';
  const SEO = {
    title,
    description,
    canonical: `${baseURL}${router.pathname}`,
    openGraph: {
      title,
      description
    }
  };
  return (
    <>
      <NextSeo {...SEO} />
      <PageHeader title="Testimonials" subTitle={false} icon="testimonials" />
      <BasicContent>
        <Testimonial
          position="left"
          leftName="Amy B. from Chicago, IL"
          leftTestimonial='"This is a 5+ excellent practice! Dr. Lim and his entire staff consistently provide excellent service that is warm, competent, confident, and patient-oriented. Highly recommended for anyone looking for comprehensive, high quality service with one-stop shopping for your dental needs."'
          rightName="Katarzyna M., Hoffman Estates, IL"
          rightTestimonial='"I went to this clinic and I had very good experience there, like someone said about the insurance that it didn’t work, I had no problem with mine. They have all up to date equipment and Dr. Nathaniel inform me about everything what need to be done before hand. I can see that he is doing his work with passion, so that makes him even better doctor. I am going to this clinic for one year now and I recommend it to everybody. For a nice big smile go to doctor Nathaniel Lim 😀"'
        />
        <Testimonial
          position="right"
          leftName="Thomas K. from Berwyn, IL"
          leftTestimonial='"I have never been “loyal” to any one doctor or dentist. My choice of provider has always been based on insurance, location or convenience. There is’nt any doctor I’d pay out of pocket for, but I found a dentist that I would pay out of pocket to see…and that’s Dr. Lim! This is based on the quality of work/service and professionalism that is consistently delivered in his practice. Dr Lim is always thorough in his assessments and proposed treatment plans. He takes as much time as needed to explain options, procedures, alternatives. His staff is caring, knowledgeable and responsive! I can (and have) recommended people to his practice with confidence."'
          rightName="Emilia S. from Chicago, IL"
          rightTestimonial='"I have recently started visiting Dr. Lim for dental work and have had nothing but great experiences. I was hesitant in the beginning however, was won over right on the first visit. He is thus far the best dentist that I have visited. I have a number of dental issues and Dr. Lim and his staff have been noting but knowledgeable and understanding about my needs; financial and otherwise. Highly recommend visiting the staff at Oak Park Dental Associates. The location is excellent, the staff is friendly and talented."'
        />
      </BasicContent>
      <FullWidthCollage />
      <PerfectSmile />
    </>
  );
}
