import { useRouter } from 'next/router';
import { NextSeo } from 'next-seo';
import PerfectSmile from '@/components/PerfectSmile';
import { PageHeader, BasicContent, BasicH2, PhoneLink } from '@/components/Common';

export default function ContactUs() {
  const router = useRouter();
  const title = 'ADA Accessibility | Oak Park Dental Associates';
  const description =
    'We strive to make our website universally accessible, and we are continuously working to improve the accessibility of content on our website. Below, you’ll find a few recommendations to help make your browsing experience more accessible.';
  const baseURL = process.env.VERCEL_URL ? process.env.VERCEL_URL : 'http://localhost:3000';
  const SEO = {
    title,
    description,
    canonical: `${baseURL}${router.pathname}`,
    openGraph: {
      title,
      description
    }
  };
  return (
    <>
      <NextSeo {...SEO} />
      <PageHeader title="ADA Accessibility Statement" subTitle={false} icon={false} />
      <BasicContent>
        <BasicH2>General</BasicH2>
        <p>
          Oak Park Dental Associates strives to ensure that its services are accessible to people
          with disabilities. Oak Park Dental Associates has invested a significant amount of
          resources to help ensure that its website is made easier to use and more accessible for
          people with disabilities, with the strong belief that every person has the right to live
          with dignity, equality, comfort and independence.
        </p>
        <BasicH2>Accessibility on Oak Park Dental Associates's Website</BasicH2>
        <p>
          Oak Park Dental Associates makes available the UserWay Website Accessibility Widget that
          is powered by a dedicated accessibility server. The software allows Oak Park Dental
          Associates's site to improve its compliance with the Web Content Accessibility Guidelines
          (WCAG 2.1).
        </p>
        <BasicH2>Enabling the Accessibility Menu</BasicH2>
        <p>
          The accessibility menu can be enabled by clicking the accessibility menu icon that appears
          on the corner of the page. After triggering the accessibility menu, please wait a moment
          for the accessibility menu to load in its entirety.
        </p>
        <BasicH2>Disclaimer</BasicH2>
        <p>
          Oak Park Dental Associates continues its efforts to constantly improve the accessibility
          of its site and services based on the belief that it is our collective moral obligation to
          allow seamless, accessible and unhindered use also for those of us with disabilities.
          Despite our efforts to make all pages and content on the site to be fully accessible, some
          content may not have yet been fully adapted to the strictest accessibility standards. This
          may be a result of not having found or identified the most appropriate technological
          solution.
        </p>
        <BasicH2>Here For You</BasicH2>
        <p>
          If you are experiencing difficulty with any content our site or require assistance with
          any part of our site, please contact us during normal business hours as detailed below and
          we will be happy to assist.
        </p>
        <BasicH2>Contact Us</BasicH2>
        <p>
          If you wish to report an accessibility issue, have any questions or need assistance,
          please contact us by calling us at <PhoneLink number={false} />.
        </p>
      </BasicContent>
      <PerfectSmile />
    </>
  );
}
