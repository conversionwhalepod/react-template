// import App from 'next/app'
import { useRouter } from 'next/router';
import { DefaultSeo } from 'next-seo';
import { ThemeProvider } from 'styled-components';
import ContextWrapper from '@/components/ContextWrapper';
import Navigation from '@/components/Navigation';
import HeroHeader from '@/components/HeroHeader';
import Header from '@/components/Header';
import Footer from '@/components/Footer';
import BookingScript from '@/components/BookingScript';
import Modal from '@/components/Modal';
import { config, footerNav } from '../content';
import SEO from '../next-seo.config';

import { GlobalStyle, theme } from '../theme';

import '../styles/main.scss';

function MyApp({ Component, pageProps }) {
  const router = useRouter();
  return (
    <>
      <ThemeProvider theme={theme}>
        <GlobalStyle />
        <DefaultSeo {...SEO} />
        <ContextWrapper configContext={config}>
          <Navigation />
          {router.pathname === '/' ? <HeroHeader /> : <Header />}
          <Component {...pageProps} />
          <Footer links={footerNav} />
          <Modal />
          <BookingScript />
        </ContextWrapper>
      </ThemeProvider>
    </>
  );
}

export default MyApp;
