import { useRouter } from 'next/router';
import { NextSeo } from 'next-seo';
import PerfectSmile from '@/components/PerfectSmile';
import { PageHeader, BasicContent, BasicH2, BasicH3 } from '@/components/Common';

export default function ContactUs() {
  const router = useRouter();
  const title = 'Privacy Policy';
  const description =
    'View our Privacy Policy. We are committed to safeguarding the privacy of your information and want you to know the protective measures we take.';
  const baseURL = process.env.VERCEL_URL ? process.env.VERCEL_URL : 'http://localhost:3000';
  const SEO = {
    title,
    description,
    canonical: `${baseURL}${router.pathname}`,
    openGraph: {
      title,
      description
    }
  };
  return (
    <>
      <NextSeo {...SEO} />
      <PageHeader title="Privacy Policy" subTitle={false} icon={false} />
      <BasicContent>
        <p>
          We know that the privacy of your personal information is important to you. We are
          committed to safeguarding the privacy of your information and want you to know the
          protective measures we take.
        </p>
        <p>
          This Website Privacy Policy concerns the information we collect when you view our website.
        </p>
        <BasicH2>Non-Public Information We Collect</BasicH2>
        <p>If you use our website, we collect the following types of information:</p>
        <BasicH3>Cookies.</BasicH3>
        <p>
          Cookies are a text file put on a user’s computer, if accepted by the computer, which
          provide to us such information as whether you are a new or returning visitor and which web
          pages you visit.
        </p>
        <BasicH3>Transparent gifs.</BasicH3>
        <p>
          Transparent gifs are an invisible image which gathers such information as where you came
          from to visit a particular web page or which of multiple links to the same page you used
          to access that page.
        </p>
        <BasicH3>Log files.</BasicH3>
        <p>
          Log files are text files on the server which gather such information as your internet
          protocol address, internet service provider and type of browser. We use this information
          to determine such things as the number of visitors and day and time of visit to our
          website.
        </p>
        <p>
          If you choose to submit your contact information to indicate an interest in a
          consultation, we receive from you your name, address, telephone number and e-mail address.
        </p>
        <p>
          We do not disclose information gained by use of cookies, transparent gifs or log files to
          affiliated or nonaffiliated third parties. This information is used internally to better
          design and manage our website.
        </p>
        <BasicH3>Information Security</BasicH3>
        <p>
          We maintain physical, electronic and procedural security measures that comply with
          applicable legal and regulatory standards to safeguard your non-public personal
          information. Access to such information is restricted to those employees who are trained
          in the proper handling of client information and have a legitimate business need to access
          that information.
        </p>
        <p>
          If you entered your contact information into our website to express an interest in being
          contacted, we use your information so that a Representative of ours may contact you to
          communicate about this opportunity.
        </p>
        <BasicH3>Our Commitment</BasicH3>
        <p>
          This privacy policy applies to our current and former customers and has been in effect
          since January 1, 2018. Because privacy is important, we pledge to work with you to protect
          and safeguard the security of your personal customer information.
        </p>
      </BasicContent>
      <PerfectSmile />
    </>
  );
}
