import { createGlobalStyle } from 'styled-components';

// Global site styles
const GlobalStyle = createGlobalStyle`

html {
  box-sizing: border-box;
  -ms-overflow-style: scrollbar;
  overflow-x: hidden;
}

*,
*::before,
*::after {
  box-sizing: inherit;
}

body {
  background-color: ${props => props.theme.colors.lightbody};
  font-family: ${props => props.theme.fonts.main};
}

body {
  background-color: ${props => props.theme.colors.lightbody};
  background-size: 100%;
  background-attachment: fixed;
  font-family: ${props => props.theme.fonts.main};
  font-weight: 300;
  font-size: 16px;
  color: ${props => props.theme.colors.secmain};
}

`;

export default GlobalStyle;
