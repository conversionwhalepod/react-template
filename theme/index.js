import GlobalStyle from './GlobalStyles';

// Global theme variables
const theme = {
  colors: {
    // CTA button
    darkcta: '#f86500',
    lightcta: '#f99923',
    // Panel Gradient
    darkmain: '#1f6c96',
    mediummain: '#5193b9',
    lightmain: '#8cb7ce',
    highlight: '#00EAF8',
    lightwedo: '#E2EDF3',
    lightbody: '#F5F9FA',
    secmain: '#333',
    darkermain: '#0F5880'
  },
  fonts: {
    main: "'Prompt', sans-serif", // General Font
    secondary: "'Barlow Condensed', sans-serif", // Font for Titles
    reviews: "'Georgia', 'Times New Roman', Times, serif" // Font for Reviews
  },
  breakpoint: {
    up: {
      sm: 'min-width: 576px',
      md: 'min-width: 768px',
      lg: 'min-width: 992px',
      xl: 'min-width: 1200px',
      xxl: 'min-width: 1440px',
      xxxl: 'min-width: 1640px'
    },
    down: {
      sm: 'max-width: 575.98px',
      md: 'max-width: 767.98px',
      lg: 'max-width: 991.98px',
      xl: 'max-width: 1199.98px',
      xxl: 'max-width: 1440px',
      xxxl: 'max-width: 1640px'
    }
  }
};

theme.gradients = {
  primary: `linear-gradient(to top, ${theme.colors.lightmain} 0%, ${theme.colors.mediummain} 59%, ${
    theme.colors.darkmain
  } 100%)`
};

export { GlobalStyle, theme };
